import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    property var rootProp: "rootProp"
    property real defaultHeight: 50

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Item {
        id: rootItem
        property var defaultColor: "blue"
        //property real defaultHeight: 50
        height: 200
        width: parent.width

        ListView {
            property real randomInteger: Math.random()
            anchors.fill: parent
            model: [0, 1, 2]
            Component.onCompleted: {
                print("root prop is available and = " + rootProp)
            }

            delegate: Component {
                Rectangle {
                    id: root
                    property var rootProp: "rootProp in delegate *** " // overwrite definition of rootProp
                    height: defaultHeight
                    width: ListView.view.width
                    color: Math.random() > 0.5 ? "yellow" : "green"
                    Text {
                        anchors.fill: parent
                        text: root.ListView.view.randomInteger + //attached properties are available to root item only. need direct reference to root
                              "\nroot prop is  available as it is in Component scope = " + rootProp +
                              "\ncomp1 prop is = " + comp1.comp1Prop +//available inly with direct id reference
                              "" //The component scope is the union of the object ids within the component and the component's root object's properties.
                    }

                }
            }
        }
        Component1 {
            id: comp1
            property string comp1Prop: "comp1Prop string " + rootProp //rootProp is the prop of very root component
            anchors.bottom: parent.bottom //binding scope object "parent"

        }
    }
}

